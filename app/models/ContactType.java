package models;

import play.db.jpa.JPA;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@NamedQueries({
        @NamedQuery(name = "ContactType.types", query = "FROM ContactType")
})

@Entity
@Table(name="comm_device_type")
public class ContactType {
    @Id
    @Column(name = "comm_device_type")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "s_comm_device_type")
    @SequenceGenerator(name = "s_comm_device_type", sequenceName = "s_comm_device_type", allocationSize = 1)
    public BigDecimal id;

    @Column(name = "name")
    public String name;

    public static List<ContactType> types() {
        return JPA.em().createNamedQuery("ContactType.types", ContactType.class).getResultList();
    }
}
