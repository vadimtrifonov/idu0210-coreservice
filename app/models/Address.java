package models;

import play.db.jpa.JPA;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@NamedQueries({
        @NamedQuery(name = "Address.addresses", query = "FROM Address a WHERE a.customerId = :customerId ORDER BY a.id")
})

@Entity
@Table(name="cst_address")
public class Address {
    @Id
    @Column(name = "cst_address")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "s_cst_address")
    @SequenceGenerator(name = "s_cst_address", sequenceName = "s_cst_address", allocationSize = 1)
    public BigDecimal id;

    @Column(name = "zip")
    public String postIndex;

    @Column(name = "house")
    public String house;

    @Column(name = "address")
    public String address;

    @Column(name = "town_county")
    public String city;

    @Column(name = "county")
    public String county;

    @Column(name = "customer")
    public BigDecimal customerId;

    public static List<Address> addresses(Long customerId) {
        return JPA.em().createNamedQuery("Address.addresses", Address.class)
                .setParameter("customerId", BigDecimal.valueOf(customerId))
                .getResultList();
    }

    public static Address findById(Long id) {
        return JPA.em().find(Address.class, BigDecimal.valueOf(id));
    }

    public void update(Long customerId, Long id) {
        this.customerId = BigDecimal.valueOf(customerId);
        this.id = BigDecimal.valueOf(id);
        JPA.em().merge(this);
    }

    public void save(Long customerId) {
        this.customerId = BigDecimal.valueOf(customerId);
        JPA.em().persist(this);
    }

    public void delete() {
        JPA.em().remove(this);
    }
}
