package models;

import play.db.jpa.JPA;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@NamedQueries({
        @NamedQuery(name = "Contact.contacts", query = "FROM Contact c WHERE c.customerId = :customerId ORDER BY c.order")
})

@Entity
@Table(name="comm_device")
public class Contact {
    @Id
    @Column(name = "comm_device")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "s_comm_device")
    @SequenceGenerator(name = "s_comm_device", sequenceName = "s_comm_device", allocationSize = 1)
    public BigDecimal id;

    @Column(name = "comm_device_type")
    public BigDecimal type;

    @Column(name = "value_text")
    public String value;

    @Column(name = "orderb")
    public BigDecimal order;

    @Column(name = "customer")
    public BigDecimal customerId;

    public static List<Contact> contacts(Long customerId) {
        return JPA.em().createNamedQuery("Contact.contacts", Contact.class)
                .setParameter("customerId", BigDecimal.valueOf(customerId))
                .getResultList();
    }

    public static Contact findById(Long id) {
        return JPA.em().find(Contact.class, BigDecimal.valueOf(id));
    }

    public void update(Long customerId, Long id) {
        this.customerId = BigDecimal.valueOf(customerId);
        this.id = BigDecimal.valueOf(id);
        JPA.em().merge(this);
    }

    public void save(Long customerId) {
        this.customerId = BigDecimal.valueOf(customerId);
        JPA.em().persist(this);
    }

    public void delete() {
        JPA.em().remove(this);
    }
}
