package models;

import play.db.jpa.JPA;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@NamedQueries({
        @NamedQuery(name = "ContractStatusType.types", query = "FROM ContractStatusType")
})

@Entity
@Table(name="contract_status_type")
public class ContractStatusType {
    @Id
    @Column(name = "contract_status_type")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "s_contract_status_type")
    @SequenceGenerator(name = "s_contract_status_type", sequenceName = "s_contract_status_type", allocationSize = 1)
    public BigDecimal id;

    @Column(name = "name")
    public String name;

    public static List<ContractStatusType> types() {
        return JPA.em().createNamedQuery("ContractStatusType.types", ContractStatusType.class).getResultList();
    }
}
