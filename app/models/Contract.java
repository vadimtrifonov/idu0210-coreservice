package models;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import play.db.jpa.JPA;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@NamedQueries({
        @NamedQuery(name = "Contract.contracts", query = "FROM Contract c")
})

@Entity
@Table(name="contract")
public class Contract {
    @Id
    @Column(name = "contract")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "s_contract")
    @SequenceGenerator(name = "s_contract", sequenceName = "s_contract", allocationSize = 1)
    public BigDecimal id;

    @Column(name = "contract_status_type")
    public BigDecimal statusType;

    @Column(name = "cnt_number")
    public String contractNumber;

    @Column(name = "name")
    public String name;

    @Column(name = "description")
    public String description;

    @Column(name = "valid_from")
    @JsonSerialize(using = DateJsonSerializer.class)
    public Date validFrom;

    @Column(name = "valid_to")
    @JsonSerialize(using = DateJsonSerializer.class)
    public Date validTo;

    @Column(name = "value_amount")
    public BigDecimal valueAmount;

    public static List<Contract> contracts() {
        return JPA.em().createNamedQuery("Contract.contracts", Contract.class).getResultList();
    }

    public static Contract findById(Long id) {
        return JPA.em().find(Contract.class, BigDecimal.valueOf(id));
    }

    public void update(Long id) {
        this.id = BigDecimal.valueOf(id);
        JPA.em().merge(this);
    }

    public void save() {
        JPA.em().persist(this);
    }

    public void delete() {
        JPA.em().remove(this);
    }
}