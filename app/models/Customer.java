package models;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import play.db.jpa.JPA;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@NamedQueries({
        @NamedQuery(name = "Customer.customers", query = "FROM Customer c")
})

@Entity
@Table(name="customer")
public class Customer {
    @Id
    @Column(name = "customer")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customer_id")
    @SequenceGenerator(name = "customer_id", sequenceName = "customer_id", allocationSize = 1)
    public BigDecimal id;

    @Column(name = "first_name")
    public String firstName;

    @Column(name = "last_name")
    public String lastName;

    @Column(name = "identity_code")
    public String identityCode;

    @Column(name = "note")
    public String note;

    @Column(name = "birth_date")
    @JsonSerialize(using = DateJsonSerializer.class)
    public Date birthDate;

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy="customerId")
    @JsonIgnore
    public List<Address> addresses;

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy="customerId")
    @JsonIgnore
    public List<Contact> contacts;

    public static List<Customer> customers() {
        return JPA.em().createNamedQuery("Customer.customers", Customer.class).getResultList();
    }

    public static Customer findById(Long id) {
        return JPA.em().find(Customer.class, BigDecimal.valueOf(id));
    }

    public void update(Long id) {
        this.id = BigDecimal.valueOf(id);
        JPA.em().merge(this);
    }

    public void save() {
        JPA.em().persist(this);
    }

    public void delete() {
        JPA.em().remove(this);
    }
}
