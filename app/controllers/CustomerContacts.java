package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Contact;
import models.ContactType;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.math.BigDecimal;

import static play.libs.Json.toJson;

public class CustomerContacts extends Controller {

    @Transactional(readOnly = true)
    public static Result list(Long customerId) {
        return ok(
            toJson(Contact.contacts(customerId))
        );
    }

    @Transactional
    @BodyParser.Of(BodyParser.Json.class)
    public static Result create(Long customerId) {
        JsonNode json = request().body().asJson();
        Contact contact = Json.fromJson(json, Contact.class);
        contact.save(customerId);

        return created(
            toJson(contact)
        );
    }

    @Transactional
    @BodyParser.Of(BodyParser.Json.class)
    public static Result update(Long customerId, Long id) {
        JsonNode json = request().body().asJson();
        Contact contact = Json.fromJson(json, Contact.class);
        contact.update(customerId, id);

        return ok(
            toJson(contact)
        );
    }

    @Transactional
    public static Result delete(Long customerId, Long id) {
        Contact contact = Contact.findById(id);
        if (contact == null || !BigDecimal.valueOf(customerId).equals(contact.customerId)) {
            return notFound();
        }
        contact.delete();

        return ok();
    }

    @Transactional(readOnly = true)
    public static Result types() {
        return ok(
            toJson(ContactType.types())
        );
    }

}
