package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Customer;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.math.BigDecimal;

import static play.libs.Json.toJson;

public class Customers extends Controller {

    @Transactional(readOnly = true)
    public static Result list() {
        return ok(
            toJson(Customer.customers())
        );
    }

    @Transactional
    @BodyParser.Of(BodyParser.Json.class)
    public static Result create() {
        JsonNode json = request().body().asJson();
        Customer customer = Json.fromJson(json, Customer.class);
        customer.save();

        return created(
            toJson(customer)
        );
    }

    @Transactional
    @BodyParser.Of(BodyParser.Json.class)
    public static Result update(Long id) {
        JsonNode json = request().body().asJson();
        Customer customer = Json.fromJson(json, Customer.class);
        customer.update(id);

        return ok(
            toJson(customer)
        );
    }

    @Transactional
    public static Result delete(Long id) {
        Customer.findById(id).delete();

        return ok();
    }
}
