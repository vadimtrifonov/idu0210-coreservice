package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Address;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.math.BigDecimal;

import static play.libs.Json.toJson;

public class CustomerAddresses extends Controller {

    @Transactional(readOnly = true)
    public static Result list(Long customerId) {
        return ok(
            toJson(Address.addresses(customerId))
        );
    }

    @Transactional
    @BodyParser.Of(BodyParser.Json.class)
    public static Result create(Long customerId) {
        JsonNode json = request().body().asJson();
        Address address = Json.fromJson(json, Address.class);
        address.save(customerId);

        return created(
            toJson(address)
        );
    }

    @Transactional
    @BodyParser.Of(BodyParser.Json.class)
    public static Result update(Long customerId, Long id) {
        JsonNode json = request().body().asJson();
        Address address = Json.fromJson(json, Address.class);
        address.update(customerId, id);

        return ok(
            toJson(address)
        );
    }

    @Transactional
    public static Result delete(Long customerId, Long id) {
        Address address = Address.findById(id);
        if (address == null || !BigDecimal.valueOf(customerId).equals(address.customerId)) {
            return notFound();
        }
        address.delete();

        return ok();
    }
}
