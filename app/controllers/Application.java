package controllers;

import models.ContractStatusType;
import models.Customer;
import models.Contract;
import org.joda.time.format.ISODateTimeFormat;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.index;

import java.math.BigDecimal;

public class Application extends Controller {

    @Transactional
    public static Result index() {
        if (Customer.customers().isEmpty()) {
            Customer jon = new Customer();
            jon.firstName = "Jon";
            jon.lastName = "Snow";
            jon.identityCode = "283";
            jon.note = "Lord Snow";
            jon.birthDate = ISODateTimeFormat.date().parseLocalDate("1983-02-01").toDate();
            jon.save();

            Customer tyrion = new Customer();
            tyrion.firstName = "Tyrion";
            tyrion.lastName = "Lannister";
            tyrion.identityCode = "274";
            tyrion.note = "The Imp";
            tyrion.birthDate = ISODateTimeFormat.date().parseLocalDate("1974-02-01").toDate();
            tyrion.save();
        }

        if (Contract.contracts().isEmpty()) {
            Contract jonContract = new Contract();
            jonContract.name = "Jon's contract";
            jonContract.contractNumber = "J1";
            jonContract.statusType = BigDecimal.valueOf(3);
            jonContract.valueAmount = BigDecimal.valueOf(100.00);
            jonContract.save();

            Contract tyrionContract = new Contract();
            tyrionContract.name = "Tyrion's contract";
            tyrionContract.contractNumber = "T2";
            tyrionContract.statusType = BigDecimal.valueOf(3);
            tyrionContract.valueAmount = BigDecimal.valueOf(10000.00);
            tyrionContract.save();
        }

        if (ContractStatusType.types().isEmpty()) {
            ContractStatusType draft = new ContractStatusType();
            draft.id = BigDecimal.valueOf(1);
            draft.name = "Draft";
            JPA.em().merge(draft);

            ContractStatusType approved = new ContractStatusType();
            approved.id = BigDecimal.valueOf(2);
            approved.name = "Approved";
            JPA.em().merge(approved);

            ContractStatusType active = new ContractStatusType();
            active.id = BigDecimal.valueOf(3);
            active.name = "Active";
            JPA.em().merge(active);

            ContractStatusType suspended = new ContractStatusType();
            suspended.id = BigDecimal.valueOf(4);
            suspended.name = "Suspended";
            JPA.em().merge(suspended);

            ContractStatusType cancelled = new ContractStatusType();
            cancelled.id = BigDecimal.valueOf(5);
            cancelled.name = "Cancelled";
            JPA.em().merge(cancelled);

            ContractStatusType superseded = new ContractStatusType();
            superseded.id = BigDecimal.valueOf(6);
            superseded.name = "Superseded";
            JPA.em().merge(superseded);

            ContractStatusType deleted = new ContractStatusType();
            deleted.id = BigDecimal.valueOf(7);
            deleted.name = "Deleted";
            JPA.em().merge(deleted);
        }

        return ok(index.render("Core service", "Listening..."));
    }


}