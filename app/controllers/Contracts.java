package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Contract;
import models.ContractStatusType;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.math.BigDecimal;
import java.util.List;

import static play.libs.Json.toJson;

public class Contracts extends Controller {

    @Transactional(readOnly = true)
    public static Result list() {
        return ok(
            toJson(Contract.contracts())
        );
    }

    @Transactional
    public static Result find(Long id) {
        return ok(
            toJson(Contract.findById(id))
        );
    }

    @Transactional
    @BodyParser.Of(BodyParser.Json.class)
    public static Result create() {
        JsonNode json = request().body().asJson();
        Contract contract = Json.fromJson(json, Contract.class);
        contract.save();

        return created(
            toJson(contract)
        );
    }

    @Transactional
    @BodyParser.Of(BodyParser.Json.class)
    public static Result update(Long id) {
        JsonNode json = request().body().asJson();
        Contract contract = Json.fromJson(json, Contract.class);
        contract.update(id);

        return ok(
            toJson(contract)
        );
    }

    @Transactional
    public static Result delete(Long id) {
        Contract.findById(id).delete();

        return ok();
    }

    @Transactional(readOnly = true)
    public static Result statusTypes() {
        return ok(
            toJson(ContractStatusType.types())
        );
    }
}
