import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

    val appName         = "CoreService"
    val appVersion      = "1.0"

    val appDependencies = Seq(
      javaCore,
      javaJdbc,
      javaJpa,
      "org.postgresql" % "postgresql" % "9.2-1003-jdbc4",
      "org.hibernate" % "hibernate-entitymanager" % "4.2.6.Final"
    )

    val main = play.Project(appName, appVersion, appDependencies).settings(
      ebeanEnabled := false
    )

}
